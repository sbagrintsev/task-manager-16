package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

    @Override
    public Project create(final String name, String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        final List<Project> result = new ArrayList<>(projects);
        result.sort(comparator);
        return result;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project findOneById(final String id) throws ProjectNotFoundException {
        for (final Project project : projects) {
            if (project.getId().equals(id)) {
                return project;
            }
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public boolean existsById(final String id) throws ProjectNotFoundException {
        return findOneById(id) != null;
    }

    @Override
    public Project remove(final Project project) {
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) throws ProjectNotFoundException {
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        return remove(project);
    }

    @Override
    public Project removeById(final String id) throws ProjectNotFoundException {
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        return remove(project);
    }

    @Override
    public int projectCount() {
        return projects.size();
    }

    @Override
    public void clear() {
        projects.clear();
    }
}
