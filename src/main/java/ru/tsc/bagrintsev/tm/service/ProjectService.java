package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectService;
import ru.tsc.bagrintsev.tm.comparator.DateCreatedComparator;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.Date;
import java.util.List;

public class ProjectService implements IProjectService {


    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(final String name) throws NameIsEmptyException {
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        return projectRepository.create(name);
    }

    @Override
    public Project create(final String name, String description) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        return projectRepository.create(name, description);
    }

    @Override
    public Project add(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) {
            return projectRepository.findAll(DateCreatedComparator.INSTANCE);
        }
        return projectRepository.findAll(sort.getComparator());
    }

    @Override
    public Project findOneByIndex(final Integer index) throws IncorrectIndexException {
        if (index == null
                || index < 0
                || index > projectRepository.projectCount()) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) throws AbstractException {
        if (index == null
                || index < 0
                || index > projectRepository.projectCount()) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeProjectByIndex(final Integer index) throws AbstractException {
        if (index == null ||
                index < 0 ||
                index > projectRepository.projectCount()) throw new IncorrectIndexException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeProjectById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        return projectRepository.removeById(id);
    }

    @Override
    public Project remove(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.remove(project);
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) throws AbstractException {
        if (index == null
                || index < 0
                || index > projectRepository.projectCount()) throw new IncorrectIndexException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            project.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            project.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            project.setDateStarted(null);
            project.setDateFinished(null);
        }
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            project.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            project.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            project.setDateStarted(null);
            project.setDateFinished(null);
        }
        return project;
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
