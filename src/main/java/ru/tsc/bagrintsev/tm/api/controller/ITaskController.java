package ru.tsc.bagrintsev.tm.api.controller;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.*;

import java.io.IOException;

public interface ITaskController {

    void showTaskList() throws IOException, IncorrectStatusException;

    void showTaskListByProjectId() throws IOException, IdIsEmptyException;

    void clearTasks();

    void createTask() throws IOException, AbstractException;

    void removeTaskByIndex() throws IOException, AbstractException;

    void removeTaskById() throws IOException, AbstractException;

    void showTaskByIndex() throws IOException, IncorrectIndexException;

    void showTaskById() throws IOException, IdIsEmptyException;

    void updateTaskByIndex() throws IOException, AbstractException;

    void updateTaskById() throws IOException, AbstractException;

    void changeTaskStatusByIndex() throws IOException, AbstractException;

    void changeTaskStatusById() throws IOException, AbstractException;

    void startTaskByIndex() throws IOException, AbstractException;

    void startTaskById() throws IOException, AbstractException;

    void completeTaskByIndex() throws IOException, AbstractException;

    void completeTaskById() throws IOException, AbstractException;

}
