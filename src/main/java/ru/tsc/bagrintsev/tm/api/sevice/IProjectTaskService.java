package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId) throws IdIsEmptyException, TaskNotFoundException, ProjectNotFoundException, AbstractException;

    void unbindTaskFromProject(String projectId, String taskId) throws IdIsEmptyException, ProjectNotFoundException, TaskNotFoundException, AbstractException;

    void removeProjectById(String projectId) throws IdIsEmptyException, TaskNotFoundException, ProjectNotFoundException, AbstractException;

}
