package ru.tsc.bagrintsev.tm.controller;

import ru.tsc.bagrintsev.tm.api.controller.ITaskController;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.util.DateUtil;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTaskList() throws IOException {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT TYPE: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortValue = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortValue);
        if (sort == null) {
            System.out.println("Sort type is wrong, applied default sort");
        }
        final List<Task> tasks = taskService.findAll(sort);
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(task);
        }
    }

    @Override
    public void showTaskListByProjectId() throws IOException, IdIsEmptyException {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        final Map<Integer, Task> tasks = taskService.findAllByProjectId(projectId);
        for (final Map.Entry<Integer, Task> taskEntry : tasks.entrySet()) {
            final int index = taskEntry.getKey() + 1;
            final Task task = taskEntry.getValue();
            if (task == null) continue;
            System.out.println(index + ". " + task);
        }
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
    }

    @Override
    public void createTask() throws IOException, AbstractException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
    }

    @Override
    public void removeTaskByIndex() throws IOException, AbstractException {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.removeTaskByIndex(index);
    }

    @Override
    public void removeTaskById() throws IOException, AbstractException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        taskService.removeTaskById(id);
    }

    @Override
    public void showTaskByIndex() throws IOException, IncorrectIndexException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        showTask(task);
    }

    @Override
    public void showTaskById() throws IOException, IdIsEmptyException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        showTask(task);
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getDateCreated()));
        System.out.println("STARTED: " + DateUtil.toString(task.getDateStarted()));
        System.out.println("FINISHED: " + DateUtil.toString(task.getDateFinished()));
    }

    @Override
    public void updateTaskByIndex() throws IOException, AbstractException {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.updateByIndex(index, name, description);
    }

    @Override
    public void updateTaskById() throws IOException, AbstractException {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        taskService.updateById(id, name, description);
    }

    @Override
    public void changeTaskStatusByIndex() throws IOException, AbstractException {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        taskService.changeTaskStatusByIndex(index, status);
    }

    @Override
    public void changeTaskStatusById() throws IOException, AbstractException {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        taskService.changeTaskStatusById(id, status);
    }

    @Override
    public void startTaskByIndex() throws IOException, AbstractException {
        System.out.println("[START TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.changeTaskStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void startTaskById() throws IOException, AbstractException {
        System.out.println("[START TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void completeTaskByIndex() throws IOException, AbstractException {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.changeTaskStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void completeTaskById() throws IOException, AbstractException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        taskService.changeTaskStatusById(id, Status.COMPLETED);
    }

}
