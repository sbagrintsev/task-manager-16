package ru.tsc.bagrintsev.tm.controller;

import ru.tsc.bagrintsev.tm.api.controller.IProjectController;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectService;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectTaskService;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.util.DateUtil;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showProjectList() throws IOException {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT TYPE: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortValue = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortValue);
        if (sort == null) {
            System.out.println("Sort type is wrong, applied default sort");
        }
        final List<Project> projects = projectService.findAll(sort);
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(project);
        }
    }

    @Override
    public void clearProjects() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
    }

    @Override
    public void createProject() throws IOException, AbstractException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
    }

    @Override
    public void removeProjectByIndex() throws IOException, AbstractException {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        projectTaskService.removeProjectById(project.getId());

    }

    @Override
    public void removeProjectById() throws IOException, AbstractException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        projectTaskService.removeProjectById(project.getId());
    }

    @Override
    public void showProjectByIndex() throws IOException, IncorrectIndexException {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        showProject(project);
    }

    @Override
    public void showProjectById() throws IOException, AbstractException {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        showProject(project);
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getDateCreated()));
        System.out.println("STARTED: " + DateUtil.toString(project.getDateStarted()));
        System.out.println("FINISHED: " + DateUtil.toString(project.getDateFinished()));
    }

    @Override
    public void updateProjectByIndex() throws IOException, AbstractException {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.updateByIndex(index, name, description);
    }

    @Override
    public void updateProjectById() throws IOException, AbstractException {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        projectService.updateById(id, name, description);
    }

    @Override
    public void changeProjectStatusByIndex() throws IOException, AbstractException {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusByIndex(index, status);
    }

    @Override
    public void changeProjectStatusById() throws IOException, AbstractException {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusById(id, status);
    }

    @Override
    public void startProjectByIndex() throws IOException, AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void startProjectById() throws IOException, AbstractException {
        System.out.println("[START PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void completeProjectByIndex() throws IOException, AbstractException {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeProjectStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void completeProjectById() throws IOException, AbstractException {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatusById(id, Status.COMPLETED);
    }

}
