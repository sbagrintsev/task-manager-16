package ru.tsc.bagrintsev.tm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public interface TerminalUtil {

    BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));

    static String nextLine() throws IOException {
        return READER.readLine();
    }

    static Integer nextNumber() throws IOException {
        return Integer.parseInt(READER.readLine());
    }

    static Date nextDate() throws IOException {
        return DateUtil.toDate(READER.readLine());
    }

}
