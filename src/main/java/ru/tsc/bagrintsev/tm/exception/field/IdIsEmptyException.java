package ru.tsc.bagrintsev.tm.exception.field;

public final class IdIsEmptyException extends AbstractFieldException {

    public IdIsEmptyException() {
        super("Error! Id is empty...");
    }

}
